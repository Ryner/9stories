import React, { Component } from 'react';
import styles from './editor.module.styl'

class ColorLayer extends Component {
    render() {
        let colorsRaw = ['#fff', '#000000', '#3897f1', '#70c04f', '#fdcb5c', '#ed4a57', '#d00769', '#a306bb']
        let colors = colorsRaw.map((item) => {
            return (
                <div onClick={() => this.props.changeColor(item)} key={item} style={{background: item}} className={styles.color}></div>
            )
        })

        return (
            <div className={styles.fieldContainer}>
                <h3>{this.props.label}</h3>
                <div className={`field-content ${styles.colorContent}`}>
                    {colors}
                </div>
            </div>
        );
    }
}

export default ColorLayer;